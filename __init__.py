# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import invoice
from . import product


def register():
    Pool.register(
        invoice.InvoiceLine,
        product.Category,
        product.CategoryAccount,
        # invoice.Invoice,
        # invoice.PartyRevenue,
        module='account_invoice_party_revenue', type_='model')
