# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval

conversor = None
try:
    from numword import numword_es
    conversor = numword_es.NumWordES()
except:
    print("Warning: Does not possible import numword module, please install it...!")

dom = ['payable', 'revenue']


class InvoiceLine(metaclass=PoolMeta):
    __name__ = 'account.invoice.line'

    party_revenue = fields.Many2One('party.party', 'Party Revenue', states={
        'invisible': Eval('type') != 'line',
        },)

    @classmethod
    def __setup__(cls):
        super(InvoiceLine, cls).__setup__()
        cls.account.domain = [
            'OR',
            ('kind', 'in', dom),
            ('company', '=', Eval('company', -1)),
            ]

    def get_move_lines(self):
        lines = super(InvoiceLine, self).get_move_lines()
        if self.party_revenue:
            for line in lines:
                line.party = self.party_revenue
        return lines


# class Invoice:
#     __metaclass__ = PoolMeta
#     __name__ = 'account.invoice'
#     revenue_received_parties = fields.Boolean('Revenue Received for Parties')
#     account_revenue_parties = fields.Many2One('account.account', 'Account Revenue Parties',
#         states={
#             'invisible': Eval('type') != 'out',
#             'invisible': Not(Bool(Eval('revenue_received_parties'))),
#             'readonly': Eval('state').in_(['validated', 'posted', 'paid'])},
#         depends= ['revenue_received_parties', 'company'],
#         domain=[
#             ('company', '=', Eval('company', -1)),
#             ('kind', '!=', 'view')
#             ])
#     percent_revenue_parties = fields.One2Many('account.invoice.party_revenue', 'invoice', 'Percent Revenue Parties', states={
#         'invisible': Eval('type') != 'out',
#         'invisible': Not(Bool(Eval('revenue_received_parties'))),
#         'readonly': Eval('state').in_(['validated', 'posted', 'paid'])
#         },
#         depends= ['revenue_received_parties'],)
#
#     @classmethod
#     def __setup__(cls):
#         super(Invoice, cls).__setup__()
#
#     def get_move(self):
#         move = super(Invoice, self).get_move()
#         InvoiceMoveLine = Pool().get('account.invoice-account.move.line')
#         InvoiceLine = Pool().get('account.invoice.line')
#         if self.revenue_received_parties and self.percent_revenue_parties:
#             sum_amount = 0
#             move_lines = []
#             for percent_party in self.percent_revenue_parties:
#                 amount_revenue_to_party = (self.untaxed_amount * (percent_party.percent_revenue_party/100))
#                 sum_amount += amount_revenue_to_party
#                 line = self._get_move_line(self.invoice_date, amount_revenue_to_party)
#                 line.account = self.account_revenue_parties.id
#                 line.party = percent_party.party_revenue.id
#                 move_lines.append(line)
#             for line in move.lines:
#                 move_lines.append(line)
#                 if line.account.kind == 'revenue':
#                     line.credit = self.untaxed_amount - sum_amount
#             move.lines = move_lines
#         return move
#
# class PartyRevenue(ModelSQL, ModelView):
#     'Revenue Party'
#     __name__ = 'account.invoice.party_revenue'
#     invoice = fields.Many2One('account.invoice', 'Invoice',
#         required=True)
#     percent_revenue_party = fields.Numeric('Percent Revenue Party', required=True)
#     party_revenue = fields.Many2One('party.party', 'Party', required=True)
