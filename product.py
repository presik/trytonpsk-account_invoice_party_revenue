# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pyson import Eval
from trytond.pool import PoolMeta


dom = ['payable', 'revenue', 'receivable']


class Category(metaclass=PoolMeta):
    __name__ = 'product.category'

    @classmethod
    def __setup__(cls):
        super(Category, cls).__setup__()
        cls.account_revenue.domain = [
            'OR',
            ('kind', 'in', dom),
            ('company', '=', Eval('company', -1)),
            ]


class CategoryAccount(metaclass=PoolMeta):
    __name__ = 'product.category.account'

    @classmethod
    def __setup__(cls):
        super(CategoryAccount, cls).__setup__()
        cls.account_revenue.domain = [
            'OR',
            ('kind', 'in', dom),
            ('company', '=', Eval('company', -1)),
            ]
